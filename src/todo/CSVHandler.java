package todo;

import java.nio.file.Path;
import java.util.HashMap;
import java.io.IOException;

/**
 * Defines the FileHandlers basic functionality
 * 
 * @author stefan
 * @version 1.0
 *
 */
public interface CSVHandler {

	/**
	 * Exports the content of the todo store to a human readable text file including
	 * timestamp and column headers
	 * 
	 * @param p path and name of the file to be exported
	 * @param s the source of objects to be exported
	 * @param delimiter the delimiter to be used to separate the columns
	 * @throws java.io.IOException if there is any file access problem
	 */
	public void exportFile(Path p, HashMap<Integer, TodoModel> s, String delimiter) throws IOException;

	/**
	 * Writes the content of a hashmap into a machine readable csv file
	 * 
	 * @param p path and name of the file to be exported
	 * @param s the source of objects to be exported
	 * @param delimiter the delimiter to be used to separate the columns
	 * @throws java.io.IOException if there is any file access problem
	 */
	public void writeFile(Path p, HashMap<Integer, TodoModel> s, String delimiter) throws IOException;

	/**
	 * Imports a machine readable csv file into a hashmap. The file must contain
	 * keys and values.
	 * 
	 * @param p path and name of the file to be imported
	 * @return HashMap containing the imported keys and values
	 * @throws java.io.IOException if there is any file access problem
	 */
	public HashMap<Integer, TodoModel> readFile(Path p) throws IOException;

}
