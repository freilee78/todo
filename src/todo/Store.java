package todo;

/**
 * Defines the todo stores basic functionality
 *
 */
public interface Store {

	/**
	 * Adding an item to the store
	 * 
	 * @param o - the object to be added
	 */
	public void addItem(TodoModel o);

	/**
	 * Retrieving and removing an item from the store
	 * 
	 * @param id - the key of the item to be removed
	 * @return the item from the store
	 */
	public TodoModel removeItem(int id);

	/**
	 * Retrieving an item from the store
	 * 
	 * @param id - the key of the item to be retrieved
	 * @return the item from the store
	 */
	public TodoModel getItem(int id);
	// public Iterable<T> getStore();

	/**
	 * Counts all the items present in the store
	 * 
	 * @return the number of items
	 */
	public int countItems();

}
