package todo;

import java.util.HashMap;

/**
 * Implementation of of the store to keep the todo items, using a HashMap
 * 
 * @author stefan
 * @version 1.0
 *
 */
public class StoreImpl implements Store {

	private static int id = 0;
	HashMap<Integer, TodoModel> store = new HashMap<>();

	/**
	 * Default constructor
	 */
	public StoreImpl() {

	}

	/**
	 * Returns the entire store as a HashMap. The Integer represents the ID of the
	 * todo items
	 * 
	 * @return the todo store
	 */
	public HashMap<Integer, TodoModel> getStore() {
		return store;
	}

	/**
	 * Adds a todo item to the store using a unique ID (HashMap key) provided by the
	 * getId() method
	 * 
	 * @param o the todo item to be added to the store
	 */
	@Override
	public void addItem(TodoModel o) {
		store.put(getId(), o);
	}

	/**
	 * Finds and returns a single todo item from the store, using the provided ID
	 * 
	 * @param id the ID of the required todo item (HashMap key)
	 * @return the TodoModel object bearing the required ID
	 */
	@Override
	public TodoModel getItem(int id) {
		return store.get(id);
	}

	/**
	 * Finds and returns a single todo item from the store, using the provided ID.
	 * Finally the todo item is removed from the store.
	 * 
	 * @param id the ID of the required todo item (HashMap key)
	 * @return TodoModel object bearing the required ID
	 */
	@Override
	public TodoModel removeItem(int id) {
		return store.remove(id);
	}

	/**
	 * Returns consecutive numbers for creating new todo items with a unique ID
	 * 
	 * @return int the next ID for creating new todo items
	 */
	public static int getId() {
		return id++;
	}

	/**
	 * Returns the current number of todo items in the store
	 * 
	 * @return int number of todo items
	 */
	@Override
	public int countItems() {
		return store.size();
	}

}
