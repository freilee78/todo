package todo;

/**
 * TodoApp containing the main method, calling the model, view and controller
 * 
 * @author stefan
 * @version 1.0
 *
 */
public class TodoApp {

	private TodoController controller;
	private TodoGUI view;
	private StoreImpl model;

	/**
	 * Default constructor instantiates the MVC
	 */
	public TodoApp() {
		super();
		model = new StoreImpl();
		view = new TodoGUI();
		controller = new TodoController(model, view);
		controller.initController();
		System.out.println("App started");

	}

	/**
	 * Todo app main method, instantiates the TodoApp class
	 * 
	 * @param args currently not used
	 */
	public static void main(String[] args) {
		new TodoApp();

	}

}
