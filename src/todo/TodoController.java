package todo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.swing.JButton;
import javax.swing.JCheckBox;

/**
 * Controller class that takes care of the communication between model and view.
 * Handles events from view and calls business logic in the model (StoreImpl,
 * TodoModelImpl). Uses the helper class TodoFileHandler for file access.
 * 
 * @author stefan
 * @version 1.0
 */
public class TodoController {

	private TodoGUI view;
	private StoreImpl model;
	private TodoFileHandler saver;
	private static ActionListener listListener;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

	/**
	 * Constructs the controller referencing the model and the view
	 * 
	 * @param model StoreImpl store using a HashMap
	 * @param view  Swing based GUI
	 */
	public TodoController(StoreImpl model, TodoGUI view) {
		this.view = view;
		this.model = model;
		// creating two todo items for test purpose
		model.addItem(new TodoModel("Test1", LocalDate.parse("1978-03-23")));
		model.addItem(new TodoModel("Test2", LocalDate.parse("1976-02-28")));
		initView();
	}

	/**
	 * Provides initial values for the view
	 * <p>
	 * -Number of todos present at application start (gains relevance with
	 * implementation of the import functionality)
	 */
	public void initView() {
		view.setTodoCounter(model.countItems());
	}

	/**
	 * Initializes the controller
	 * <p>
	 * -instantiates ActionListeners for the view elements
	 * <p>
	 * -provides functionality to be executed on events
	 */
	/**
	 * 
	 */
	public void initController() {
		
		System.out.println("Controller initalized");

		saver = new TodoFileHandler();

		// ActionListener for the items list
		listListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String command = e.getActionCommand();
				System.out.println(command);
				if (command.startsWith("check")) {
					Integer itemID = Integer.valueOf(command.substring(5));
					System.out.println("Check/uncheck detected: Item " + itemID);
					model.getItem(itemID).changeDone();
					System.out.println("Aktueller Zustand: " + model.getItem(itemID).toString());
				}
				if (command.startsWith("delete")) {
					Integer itemID = Integer.valueOf(command.substring(6));
					System.out.println("Delete detected: Item " + itemID);
					System.out.println("Geloescht: " + model.removeItem(itemID).toString());
				}
				view.listStoreItems(model.getStore());
				view.refresh();
			}
		};

		view.listStoreItems(model.getStore());

		// ActionListener for "New" button
		
		view.getNewButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("New wird ausgefuehrt");
				view.switchNorthPanel("add");
			}
		});
		// ActionListener for "Save" button
		// Validierung des Datums irgendwo ganz oben oder ganz unten (hier im Controller
		// oder im TodoModel)?
		view.getSaveButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Save wird ausgefuehrt");
				try {
					model.addItem(new TodoModel(view.getTodoText().getText(),
							LocalDate.parse(view.getTodoDate().getText(), formatter)));
					view.clearTodoText();
					view.clearTodoDate();
				} catch (DateTimeParseException dtpe) {
					System.err.println("Date not valid!");
					view.dateExcMessage();
				}

				view.listStoreItems(model.getStore());
				view.setTodoCounter(model.countItems());
				view.switchNorthPanel("save");
				view.refresh();
			}
		});

		// ActionListener for "Export" button
		view.getExportButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				System.out.println("exporting to csv");
				try {
					saver.exportFile(view.getFileChooser("Export"), model.getStore(), ",");
				} catch (IOException ioe) {
					System.out.println("Presenting IO Exception Dialog");
					System.err.println(ioe.getMessage());
					ioe.printStackTrace();

				}
			}
		});

	}

	/**
	 * Static method used by the view to register the "done" checkbox of a single
	 * line to the ActionListener
	 * 
	 * @param checkbox a certain checkbox from the todo list
	 */
	public static void listenToCheckBox(JCheckBox checkbox) {
		checkbox.addActionListener(listListener);
	}

	/**
	 * Static method used by the view to register the "Delete" button of a single
	 * line to the ActionListener
	 * 
	 * @param button a certain "Delete" button from the todo list
	 */
	public static void listenToButton(JButton button) {
		button.addActionListener(listListener);
	}

}
