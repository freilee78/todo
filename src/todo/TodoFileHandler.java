package todo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

/**
 * Helper class to write contents of the todo store into text files. Coming
 * version to read a csv file into the store
 * 
 * @version 1.0
 * @author stefan
 *
 */
public class TodoFileHandler implements CSVHandler {

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
	DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

	/**
	 * Exports the content of the todo store to a human readable text file including
	 * timestamp and column headers
	 * 
	 * @param p path and name of the file to be exported
	 * @param s the todo store
	 * @param d the delimiter to be used to separate the columns
	 * @throws IOException if there is any file access problems
	 */
	@Override
	public void exportFile(Path p, HashMap<Integer, TodoModel> s, String d) throws IOException {

		FileWriter exporter = new FileWriter(p.toFile());
		BufferedWriter bw = new BufferedWriter(exporter);
		bw.write("Exported " + LocalDateTime.now().format(formatter));
		bw.write("\n");
		bw.write("ID" + d + "TodoText" + d + "Date" + d + "Done");
		bw.write("\n");
		for (Integer i : s.keySet()) {
			bw.write(
					i + d + s.get(i).getText() + d + s.get(i).getDatum().format(dateformatter) + d + s.get(i).isDone());
			bw.write("\n");

		}

		bw.flush();
		bw.close();

	}

	/**
	 * Writes the content of the todo store to a machine readable csv file
	 * 
	 * @param p path and name of the file to be exported
	 * @param s the todo store
	 * @param d the delimiter to be used to separate the columns
	 * @throws IOException if there is any file access problems
	 */
	@Override
	public void writeFile(Path p, HashMap<Integer, TodoModel> s, String d) throws IOException {
		FileWriter exporter = new FileWriter(p.toFile());
		BufferedWriter bw = new BufferedWriter(exporter);
		for (Integer i : s.keySet()) {
			bw.write(i + d + s.get(i).getText() + d + s.get(i).getDatum() + d + s.get(i).isDone());
			bw.write("\n");

		}

		bw.flush();
		bw.close();

	}

	/**
	 * not yet implemented
	 */
	@Override
	public HashMap<Integer, TodoModel> readFile(Path p) {
		// TODO Auto-generated method stub
		return null;
	}
}
