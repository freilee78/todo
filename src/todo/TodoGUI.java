package todo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * View component of the Todo App. Provides all GUI Elements, including
 * FileChooser and error messages
 * 
 * @author stefan
 * @version 1.0
 *
 */
public class TodoGUI { // implements ActionListener{

	JFrame appFrame;
	JPanel northPanelSave;
	JPanel northPanelAdd;
	JPanel middlePanel;
	JPanel southPanel;
	JTextField todoText, todoDate;
	JButton saveButton, newButton, writeButton, exportButton, readButton;
	JLabel todoCounter;
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
	// private static final String NEW_COMMAND = "new_command";
	// private static final String SAVE_COMMAND = "save_command";

	/**
	 * Draws the Todo App GUI using swing elements
	 * <p>
	 * - JFrame based with borderlayout
	 * <p>
	 * - north area with flow layout for data entry
	 * <p>
	 * - center area with box layout containing the todo items
	 * <p>
	 * - south area with function buttons
	 */
	public TodoGUI() {

		// initialize frame
		appFrame = new JFrame();
		appFrame.setTitle("Java Todo List");
		appFrame.setSize(400, 300);

		// create panel for data entry
		northPanelSave = new JPanel(new FlowLayout());
		todoText = new JTextField(8);
		todoDate = new JTextField(8);
		saveButton = new JButton("Save");
		saveButton.setActionCommand("save_command");
		northPanelSave.add(todoText);
		northPanelSave.add(todoDate);
		northPanelSave.add(saveButton);

		// create panel for counting items
		northPanelAdd = new JPanel(new FlowLayout());
		newButton = new JButton("New Todo");
		newButton.setActionCommand("new_command");

		todoCounter = new JLabel("There are 0 Todos to do!");
		northPanelAdd.add(todoCounter);
		northPanelAdd.add(newButton);

		// create middlePanel containing the todo items
		middlePanel = new JPanel();
		middlePanel.setLayout(new BoxLayout(middlePanel, BoxLayout.Y_AXIS));

		// create panel for the file operation buttons
		southPanel = new JPanel(new FlowLayout());
		exportButton = new JButton("Export to disk");
		exportButton.setActionCommand("export_command");
		southPanel.add(exportButton);

		// put panels on frame and set visible
		appFrame.add(southPanel, BorderLayout.SOUTH);
		appFrame.add(northPanelAdd, BorderLayout.NORTH);
		appFrame.add(middlePanel, BorderLayout.CENTER);
		appFrame.setVisible(true);
	}

	// Getters Setters
	// ******************************************************************/

	/**
	 * Getter for the text entry field on the panel used to create new todo items
	 * 
	 * @return JTextField the text field and its content
	 */
	public JTextField getTodoText() {
		return todoText;
	}

	/**
	 * Getter for the date field on the panel used to create new todo items
	 * 
	 * @return JTextField the text field and its content
	 */
	public JTextField getTodoDate() {
		return todoDate;
	}

	/**
	 * Getter for the "Save" button on the panel used to create new todo items
	 * 
	 * @return JButton "Save"
	 */
	public JButton getSaveButton() {
		return saveButton;
	}

	/**
	 * Getter for the "New Todo" button on the panel showing the current number of
	 * todo items
	 * 
	 * @return JButton "New Todo"
	 */
	public JButton getNewButton() {
		return newButton;
	}

	/**
	 * Getter for the "Export" button on the south panel
	 * 
	 * @return JButton "Export"
	 */
	public JButton getExportButton() {
		return exportButton;
	}

	/**
	 * Creates the text for the todo counter label
	 * 
	 * @param todoCount pass the current size of the todo store to be put in the
	 *                  label on top of the todo list
	 */
	public void setTodoCounter(int todoCount) {
		todoCounter.setText("There are " + todoCount + " Todos to do!");
	}

	// Event Handling
	// ******************************************************************
	// moved over to the controller class

	// Auxiliary Methods
	// ******************************************************************

	/**
	 * Switches between the panels in the north of the appframe. - Pass "add" to
	 * show the panel with the entry fields and the save button - Pass "save" to
	 * show the panel with the todo counter and the new button. Passed values are not
	 * validated
	 * 
	 * @param panel String values "add" or "save"
	 */
	protected void switchNorthPanel(String panel) {

		if (panel.equals("add")) {
			appFrame.remove(northPanelAdd);
			appFrame.add(northPanelSave, BorderLayout.NORTH);
			appFrame.revalidate();
			appFrame.repaint();
		}
		if (panel.equals("save")) {
			appFrame.remove(northPanelSave);
			appFrame.add(northPanelAdd, BorderLayout.NORTH);
			appFrame.revalidate();
			appFrame.repaint();
		}
	}

	/**
	 * Displays all todo items from the store in a list format. Adds the "done"
	 * checkboxes and the "delete" buttons.
	 * 
	 * @param hashMap The todo store of type "HashMap<Integer, TodoModel>"
	 */
	protected void listStoreItems(HashMap<Integer, TodoModel> hashMap) {
		middlePanel.removeAll();

		for (int i : hashMap.keySet()) {
			JPanel line = new JPanel();
			line.add(new JLabel("" + i));
			line.add(new JTextField(hashMap.get(i).getText(), 10));
			line.add(new JTextField((hashMap.get(i).getDatum().format(formatter)), 10));

			// add checkboxes on each line
			JCheckBox tickbox;
			tickbox = (new JCheckBox("Done", false));
			if (hashMap.get(i).isDone() == true) {
				tickbox.setSelected(true);
			}
			tickbox.setActionCommand("check" + i);
			TodoController.listenToCheckBox(tickbox);
			line.add(tickbox);

			JButton deleteButton;
			deleteButton = (new JButton("Delete"));
			if (hashMap.get(i).isDone() != true) {
				deleteButton.setEnabled(false);
			}
			deleteButton.setActionCommand("delete" + i);
			TodoController.listenToButton(deleteButton);
			line.add(deleteButton);
			middlePanel.add(line);

		}

		appFrame.revalidate();
		appFrame.repaint();
	}

	/**
	 * Clears the Todo text field after the value has been read
	 * 
	 */
	public void clearTodoText() {
		todoText.setText("");
	}

	/**
	 * Clears the Todo date field after the value has been read
	 * 
	 */
	public void clearTodoDate() {
		todoDate.setText("");
		;
	}

	/**
	 * Refreshes the AppFrame
	 */
	public void refresh() {
		appFrame.revalidate();
		appFrame.repaint();
	}

	/**
	 * Displays a dialog box with the message "Date is not valid, format must be
	 * dd.mm.yyyy!"
	 */
	public void dateExcMessage() {
		JFrame dateMessage = new JFrame();
		JOptionPane.showMessageDialog(dateMessage, "Date is not valid, format must be dd.mm.yyyy!");
	}

	/**
	 * Displays a FileChooser frame
	 * 
	 * @param buttontext The text for the approve button must be passed as string
	 *                   (i.e. "Open", "Export", Save"). Gains relevance with
	 *                   implementation of the import functionality
	 * @return Path The file's path and name that should be written or imported
	 */
	public Path getFileChooser(String buttontext) {
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Todo Files", "todo");
		chooser.setFileFilter(filter);
		chooser.setApproveButtonText(buttontext);
		int returnVal = chooser.showOpenDialog(appFrame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("You chose to open this file: " + chooser.getSelectedFile().getName());
			return chooser.getSelectedFile().toPath();
		}
		return null;
	}
}
