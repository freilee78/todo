package todo;

import java.time.LocalDate;

/**
 * Describes the attributes and the behaviours of the todo items. As the ID is supposed to be the
 * key of the hashmap, there is no ID field in this TodoModel implementation
 * @author stefan
 * @version 1.0
 *
 */
public class TodoModel implements Comparable<TodoModel> {
	
	//private int id;
	private String text;
	private boolean done;
	private LocalDate datum;
	
	public TodoModel(String text, LocalDate datum) {
	//	this.id = StoreImpl.getId();
		this.text = text;
		this.done = false;
		setDatum(datum);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}
	
	public void changeDone() {
		if (this.done == true) {
			this.done = false;
			System.out.println("Changed from true to false");
		} else {
			this.done = true;
			System.out.println("Changed from false to true");
		}
	}

	public LocalDate getDatum() {
		return datum;
	}

	public boolean setDatum(LocalDate datum) {
		try {
			this.datum = datum;
			return true;
		}
		catch (Exception e) {
			return false;
			
		}
		
	}

//	public int getId() {
//		return id;
//	}

	@Override
	public String toString() {
		return "TodoModel [text=" + text + ", done=" + done + ", datum=" + datum + "]"; //id=" + id + ", 
	}
	
	@Override
	public int compareTo(TodoModel o) {	
		return this.datum.compareTo(o.datum);
	}

}
